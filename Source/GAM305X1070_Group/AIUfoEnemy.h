// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AIUfoEnemy.generated.h"

UCLASS()
class GAM305X1070_GROUP_API AAIUfoEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAIUfoEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveToFlyingWaypoints();

	//void MoveToFlyingStart();

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		int CurrentFlyingWaypoint;

	TArray<AActor*> FlyingWaypoints;
};
