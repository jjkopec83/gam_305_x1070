// Fill out your copyright notice in the Description page of Project Settings.

#include "AIUfoEnemy.h"
#include "FlyingAIController.h"
#include "kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "FlyingWaypoint.h"


// Sets default values
AAIUfoEnemy::AAIUfoEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAIUfoEnemy::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFlyingWaypoint::StaticClass(), FlyingWaypoints);
	
	MoveToFlyingWaypoints();
}

// Called every frame
void AAIUfoEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*if (CurrentFlyingWaypoint == FlyingWaypoints.Num()) {		Code crashes editor, to be rewritten
		MoveToFlyingStart();
	}*/
}

// Called to bind functionality to input
void AAIUfoEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AAIUfoEnemy::MoveToFlyingWaypoints() {
	AFlyingAIController* FlyingAIController = Cast<AFlyingAIController>(GetController());
	
	if (FlyingAIController) {
		if (CurrentFlyingWaypoint <= FlyingWaypoints.Num()) {
			for (AActor* FlyingWaypoint : FlyingWaypoints) {

				AFlyingWaypoint* FlyingWaypointIterator = Cast<AFlyingWaypoint>(FlyingWaypoint);

				if (FlyingWaypointIterator) {
					if (FlyingWaypointIterator->GetFlyingWaypointOrder() == CurrentFlyingWaypoint) {

						FlyingAIController->MoveToActor(FlyingWaypointIterator, 5.f, false);

						CurrentFlyingWaypoint++;
						break;
					}

				}
			}
		}
	}
}

// Code causes crash in current state.
// To do: Re-write code to ensure AI loops its path
/////////////////////////////////////////////////////
/*void AAIUfoEnemy::MoveToFlyingStart() {
	CurrentFlyingWaypoint = 0;

	AFlyingAIController* FlyingAIController = Cast<AFlyingAIController>(GetController());

	do {
		if (FlyingAIController) {
			if (CurrentFlyingWaypoint <= FlyingWaypoints.Num()) {
				for (AActor* FlyingWaypoint : FlyingWaypoints) {

					AFlyingWaypoint* FlyingWaypointIterator = Cast<AFlyingWaypoint>(FlyingWaypoint);

					if (FlyingWaypointIterator) {
						if (FlyingWaypointIterator->GetFlyingWaypointOrder() == CurrentFlyingWaypoint) {

							FlyingAIController->MoveToActor(FlyingWaypointIterator, 5.f, false);

							CurrentFlyingWaypoint++;

						}

					}
				}
			}
		}
	} while (CurrentFlyingWaypoint < FlyingWaypoints.Num());
}*/