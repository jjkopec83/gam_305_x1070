// Fill out your copyright notice in the Description page of Project Settings.

#include "FlyingAIController.h"
#include "AIUfoEnemy.h"


void AFlyingAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) {
	AAIUfoEnemy* AIUfoEnemy = Cast<AAIUfoEnemy>(GetPawn());

	if (AIUfoEnemy) {
		AIUfoEnemy->MoveToFlyingWaypoints();
	}
}