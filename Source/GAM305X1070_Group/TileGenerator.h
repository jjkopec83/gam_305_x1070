// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "LevelTile.h"
#include "Engine/Engine.h"
#include "GameFramework/Actor.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "GAM305X1070_GroupGameMode.h"
#include "TileGenerator.generated.h"


UCLASS()
class GAM305X1070_GROUP_API ATileGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileGenerator();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gen")
		int genAmount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gen")
		float tileSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gen")
		ACharacter* followActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gen")
		ALevelTile* firstTIle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gen")
		TSubclassOf<class ALevelTile> lastTile;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gen")
		TArray<TSubclassOf<class ALevelTile>> levelTiles;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gen")
		FVector spawnLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gen")
		int branchType = 0;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gen")	//Created for compile purposes -JK
		ALevelTile* PlacedTile;

	UFUNCTION(BlueprintPure, Category = "Gen")
		float GetLevelPercent();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float StartPosition;
	float EndPosition;
	float levelProgressPercent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
