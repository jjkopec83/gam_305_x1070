// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "CampFire.generated.h"

class UBoxComponent;
class UParticleSystemComponent;

UCLASS()
class GAM305X1070_GROUP_API ACampFire : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACampFire();

public:

	// Create fire particle effct
	UPROPERTY(EditAnywhere)
	UParticleSystemComponent* Fire;

	// Create MyBoxComponent object
	UPROPERTY(EditAnywhere)
	UBoxComponent* MyBoxComponent;

	// Create FireDamageType class to handle fire damage on character
	UPROPERTY(EditAnywhere)
	TSubclassOf<UDamageType> FireDamageType;

	// Create MyCharacter based on player character
	UPROPERTY(EditAnywhere)
	AActor* MyCharacter;

	//Create MyHit to handle taking initial fire damage
	UPROPERTY(EditAnywhere)
	FHitResult MyHit;

	// Create boolean to handle whether the player can or can't be hit by fire damage
	bool bCanApplyDamage;

	FTimerHandle FireTimerHandle; // Create damage over time timer for fire damage

	// declare component overlap begin function
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// declare component overlap end function
	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Create function to allow player to take fire damage
	UFUNCTION()
	void ApplyFireDamage();
};

