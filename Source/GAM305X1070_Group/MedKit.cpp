// Fill out your copyright notice in the Description page of Project Settings.


#include "MedKit.h"
#include "GAM305X1070_GroupCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"

// Sets default values
AMedKit::AMedKit()
{
	OnActorBeginOverlap.AddDynamic(this, &AMedKit::OnOverlap); // Sets overlap event for picking up medkit objects in level environment
}

void AMedKit::OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor) // Define OnOverlap function that allows player to pick up medkit 
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		class AGAM305X1070_GroupCharacter* MyCharacter = Cast<AGAM305X1070_GroupCharacter>(OtherActor);

		// If Players health is less than 100% then update their health amount by 100 HP points upon picking up medkit
		if (MyCharacter && MyCharacter->GetHealth() < 1.0f)
		{
			MyCharacter->UpdateHealth(1000.0f); // Refills health completely to player

			UGameplayStatics::PlaySound2D(GetWorld(), Healing_Cue); // Play Healing sound effect

			//Adds score
			AGAM305X1070_GroupGameMode* gameMode = Cast<AGAM305X1070_GroupGameMode>(GetWorld()->GetAuthGameMode());
			gameMode->RemoveFromScore(FMath::FRandRange(3000, 7000));

			Destroy(); // Despawns medkit on pickup
		}
	}
}

