// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelTile.generated.h"

UCLASS()
class GAM305X1070_GROUP_API ALevelTile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALevelTile();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Level")
		int BranchType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Level")
		int TileWidth = 1;

	UFUNCTION(BlueprintCallable)
	int GetBranchType();
	
	UFUNCTION(BlueprintCallable)
	int GetTileWidth();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
