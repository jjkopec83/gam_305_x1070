// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Math/UnrealMathUtility.h"
#include "GAM305X1070_GroupGameMode.h"
#include "GAM305X1070_GroupCharacter.h"
#include "AmmoKit.generated.h"

UCLASS()
class GAM305X1070_GROUP_API AAmmoKit : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAmmoKit();

	// Create OnOverlap function to allow medkit to be picked up by player
	UFUNCTION()
	void OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor);

	// Create SoundBase property in Blueprint for AmmoKit that will allow sound effect to play when picked up
	UPROPERTY(EditAnywhere, Category = "Sound")
	USoundBase* Ammo_Refill_Cue;
};
