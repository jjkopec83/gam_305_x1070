// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelTile.h"

// Sets default values
ALevelTile::ALevelTile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

int ALevelTile::GetBranchType()
{
	return BranchType;
}

int ALevelTile::GetTileWidth()
{
	return TileWidth;
}

// Called when the game starts or when spawned
void ALevelTile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALevelTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

