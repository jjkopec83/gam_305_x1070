// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GAM305X1070_GroupCharacter.generated.h"

class UInputComponent;
class UTimelineComponent;

UCLASS(config=Game)
class AGAM305X1070_GroupCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

protected:

	/** Called for side to side input */
	void MoveRight(float Val);

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	virtual void Landed(const FHitResult& Hit) override;


public:
	AGAM305X1070_GroupCharacter();

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }


protected:
	virtual void BeginPlay();

	virtual void Tick(float DeltaTime) override;

	float TakenDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator,
		AActor* DamageCauser);

public:
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")	//Player Max Health parameter
		float MaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")	//Player Current Health parameter
		float CurrentHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")	//Player Health Percentage
		float HealthPercentage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")	//Player Max Ammo parameter
		float MaxAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")	//Player Current Ammo parameter
		float CurrentAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")	//Player Current Ammo percentage
		float AmmoPercentage;

	/** Create previous Ammo points percentage variable*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	float PreviousAmmo;

	/** Create Ammo value variable */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		float AmmoValue;

	/** Create flash effect for loss of health variable */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float redFlash;

	/** Create ammo points curve variable */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	UCurveFloat* AmmoCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health") // At the point in which fall damage kicks in
		float DamgePoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health")
		float HighPoint;


	/** Create Timeline variables */
	float CurveFloatValue;
	float TimelineValue;
	bool bCanUseAmmo; // Create variable to allow for player to use ammo when projectile is fired

	UTimelineComponent* MyTimeline; // Create Timeline object

	/** Create Timer variables */
	struct FTimerHandle MemberTimerHandle;
	struct FTimerHandle AmmoTimerHandle;

	UFUNCTION(BlueprintPure, Category = "Health")	//Get Player's Health
		float GetHealth();

	UFUNCTION(BlueprintPure, Category = "Ammo")		//Get Player's Ammo
		float GetAmmo();

	UFUNCTION(BlueprintPure, Category = "Health")	//Get Player's Health Text
		FText GetHealthIntText();

	UFUNCTION(BlueprintPure, Category = "Ammo")		//Get Player's Ammo Text
		FText GetAmmoIntText();

	/** Damage Timer */
	UFUNCTION()
	void DamageTimer();

	/** Set Damage State */
	UFUNCTION()
	void SetDamageState();

	/** Updates Ammo */
	UFUNCTION()
	void UpdateAmmo(float AmmoChange);

	/** Create function to update health upon gaining or losing it */
	UFUNCTION(BlueprintCallable, Category = "Power")
	void UpdateHealth(float HealthChange);

	/** Play Flash */
	UFUNCTION(BlueprintPure, Category = "Health")
	bool PlayFlash();

protected:

	/** Fires a projectile. */
	void OnFire();
};
