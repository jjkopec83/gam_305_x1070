// Fill out your copyright notice in the Description page of Project Settings.


#include "Fan.h"

// Sets default values
AFan::AFan()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFan::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFan::BoxTraceForPlayer()
{
	FCollisionShape box = FCollisionShape::MakeBox(FVector(50, 50, 50));
	FHitResult hit;

	FVector start = GetActorLocation();
	FVector end = start + (GetActorUpVector() * DetectDistance);

	//Ignore Own collision
	FCollisionQueryParams collisionParams;
	collisionParams.AddIgnoredActor(this);

	//Box Trace
	bool hitSomething = GetWorld()->SweepSingleByObjectType(hit, start, end, GetActorQuat(), ECC_Pawn, box, collisionParams);

	if (hitSomething && hit.GetActor()->IsA(PlayerRef)) {
		//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *hit.GetActor()->GetName()));
		AGAM305X1070_GroupCharacter *player = Cast<AGAM305X1070_GroupCharacter>(hit.GetActor()); // Get Player Actor
		FVector force;

		force = UKismetMathLibrary::GetDirectionUnitVector(GetActorLocation(), hit.GetActor()->GetActorLocation()) * (PushForce * 60); // Gets Actor Direction

		//Applies force depending on whther fan is point vertically or horizontally
		if (GetActorRotation().Roll != 0)
			force = FVector(0, force.Y, 0);
		else
			force = FVector(0, 0, force.Z);

		//Adds force to player
		player->GetCharacterMovement()->AddForce(force);
	}

}

// Called every frame
void AFan::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BoxTraceForPlayer();
}

