// Fill out your copyright notice in the Description page of Project Settings.


#include "TarTrap.h"

// Sets default values
ATarTrap::ATarTrap()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Box Component Setup
	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	TriggerBox->SetCollisionProfileName(TEXT("Trigger"));
	TriggerBox->SetupAttachment(RootComponent);

	//Overlap Function Setup
	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &ATarTrap::OnOverlapBegin);
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &ATarTrap::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ATarTrap::BeginPlay()
{
	Super::BeginPlay();

	// Set up character movement values
	UCharacterMovementComponent* character = player.GetDefaultObject()->GetCharacterMovement();
	originalJumpZVelocity = character->JumpZVelocity;
	originalWalkSpeed = character->MaxWalkSpeed;
	reducedJumpZVelocity = originalJumpZVelocity * jumpMultiplier;
	reducedWalkSpeed = originalWalkSpeed * walkSpeedMultiplier;
}

// Called every frame
void ATarTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATarTrap::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->IsA(player)) {
		UCharacterMovementComponent* character = Cast<AGAM305X1070_GroupCharacter>(OtherActor)->GetCharacterMovement();
		if (character) {
		character->JumpZVelocity = reducedJumpZVelocity;
		character->MaxWalkSpeed = reducedWalkSpeed;
		}
	}
}

void ATarTrap::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->IsA(player)) {
		UCharacterMovementComponent* character = Cast<AGAM305X1070_GroupCharacter>(OtherActor)->GetCharacterMovement();
		if (character) {
			character->JumpZVelocity = originalJumpZVelocity;
			character->MaxWalkSpeed = originalWalkSpeed;
		}
	}

}

