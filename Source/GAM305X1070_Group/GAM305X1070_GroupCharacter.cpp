// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "GAM305X1070_GroupCharacter.h"
#include "Camera/CameraComponent.h"
#include "Animation/AnimInstance.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "Kismet/KismetMathLibrary.h" // Created for HUD programming
#include "TimerManager.h" // Created for HUD programming
#include "Components/TimelineComponent.h" // Needed for Timeline programming

#include "Engine.h"

AGAM305X1070_GroupCharacter::AGAM305X1070_GroupCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->RelativeRotation = FRotator(0.f,180.f,0.f);

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

void AGAM305X1070_GroupCharacter::BeginPlay() {

	// Call the base class
	Super::BeginPlay();

	// Declare health player stats
	MaxHealth = 1000.0f;
	CurrentHealth = MaxHealth;
	HealthPercentage = 1.0f;
	bCanBeDamaged = true;

	// Declare ammo player stats
	MaxAmmo = 5.0f;
	CurrentAmmo = MaxAmmo;
	AmmoPercentage = 1.0f;
	PreviousAmmo = AmmoPercentage;
	AmmoValue = 1.0f;
	bCanUseAmmo = true;
	HighPoint = -INFINITY;

}

void AGAM305X1070_GroupCharacter::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (GetCharacterMovement()->IsFalling()) {
		if (GetActorLocation().Z > HighPoint) {
			HighPoint = GetActorLocation().Z;
		}
	}

	if (MyTimeline != nullptr) MyTimeline->TickComponent(DeltaTime, ELevelTick::LEVELTICK_TimeOnly, nullptr);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AGAM305X1070_GroupCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGAM305X1070_GroupCharacter::MoveRight);

	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &AGAM305X1070_GroupCharacter::OnFire);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &AGAM305X1070_GroupCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AGAM305X1070_GroupCharacter::TouchStopped);
}

void AGAM305X1070_GroupCharacter::Landed(const FHitResult& Hit)
{
	float fallDistance = HighPoint - GetActorLocation().Z;
	if (fallDistance > DamgePoint) {
		UpdateHealth(UKismetMathLibrary::NormalizeToRange(fallDistance, DamgePoint, 3000) * MaxHealth * -1);
		HighPoint = -INFINITY;
	}
}

void AGAM305X1070_GroupCharacter::OnFire()
{
	if (!FMath::IsNearlyZero(CurrentAmmo, 0.001f) && bCanUseAmmo)
	{
		if (MyTimeline != nullptr) MyTimeline->Stop();
		GetWorldTimerManager().ClearTimer(AmmoTimerHandle); // Clear Timer to allow gun to fire again

		&AGAM305X1070_GroupCharacter::UpdateAmmo;
	}
}
void AGAM305X1070_GroupCharacter::MoveRight(float Value)
{
	// add movement in that direction
	AddMovementInput(FVector(0.f,-1.f,0.f), Value);
}

void AGAM305X1070_GroupCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// jump on any touch
	Jump();
}

void AGAM305X1070_GroupCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}

float AGAM305X1070_GroupCharacter::GetHealth() {	//Returns health percentage for HUD
	return HealthPercentage;
}

float AGAM305X1070_GroupCharacter::GetAmmo() {	//Returns ammo percentage for HUD
	AmmoPercentage = CurrentAmmo / MaxAmmo;
	return AmmoPercentage;
}

FText AGAM305X1070_GroupCharacter::GetHealthIntText() {		//Converts health value to text to use in HUD
	int32 HP = FMath::RoundHalfFromZero(HealthPercentage * 100);
	FString HPS = FString::FromInt(HP);
	FString HealthHUD = HPS + FString(TEXT("%"));
	FText HPText = FText::FromString(HealthHUD);
	return HPText;
}

FText AGAM305X1070_GroupCharacter::GetAmmoIntText() {	//Converts ammo value to text to use in HUD
	FString AMMO = FString::FromInt(CurrentAmmo);
	FString FullAmmo = FString::FromInt(MaxAmmo);
	FString AmmoHUD = AMMO + FString(TEXT("/")) + FullAmmo;
	FText AmmoText = FText::FromString(AmmoHUD);
	return AmmoText;
}

// Sets player can be damaged
void AGAM305X1070_GroupCharacter::SetDamageState()
{
	bCanBeDamaged = true;
}

// Defines how long the player takes damage for
void AGAM305X1070_GroupCharacter::DamageTimer()
{
	GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &AGAM305X1070_GroupCharacter::SetDamageState, 2.0f, false);
}


// Plays flash when player takes damage
bool AGAM305X1070_GroupCharacter::PlayFlash()
{
	if (redFlash)
	{
		redFlash = false;
		return true;
	}

	return false;
}

// Handles math that calculates health left after taking damage
void AGAM305X1070_GroupCharacter::UpdateHealth(float HealthChange)
{
	CurrentHealth = FMath::Clamp(CurrentHealth += HealthChange, 0.0f, MaxHealth);
	HealthPercentage = CurrentHealth / MaxHealth;
}

// Handles math that calculates ammo left after using some
void AGAM305X1070_GroupCharacter::UpdateAmmo(float AmmoChange)
{
	CurrentAmmo += AmmoChange;
	
	if (CurrentAmmo > 5) {
		CurrentAmmo = 5;
	}

	AmmoPercentage = CurrentAmmo / MaxAmmo;
}
