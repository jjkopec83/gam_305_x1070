// Fill out your copyright notice in the Description page of Project Settings.


#include "ElectricField.h"

// Sets default values
AElectricField::AElectricField()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Sphere Component Setup
	TriggerSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Trigger Sphere"));
	TriggerSphere->SetCollisionProfileName(TEXT("Trigger"));
	TriggerSphere->SetupAttachment(RootComponent);

	//Overlap Function Setup
	TriggerSphere->OnComponentBeginOverlap.AddDynamic(this, &AElectricField::OnOverlapBegin);
	TriggerSphere->OnComponentEndOverlap.AddDynamic(this, &AElectricField::OnOverlapEnd);

}

// Called when the game starts or when spawned
void AElectricField::BeginPlay()
{
	Super::BeginPlay();

}


// Called every frame
void AElectricField::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AElectricField::DamagePlayer()
{
	if(character)
		character->UpdateHealth(-tickDamage);
}

void AElectricField::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->IsA(player)) {
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("True")));
		character = Cast<AGAM305X1070_GroupCharacter>(OtherActor);
		//Start Timer
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AElectricField::DamagePlayer, 1.0f, true, 0);
	}
}

void AElectricField::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->IsA(player)) {
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("False")));
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	}
}
