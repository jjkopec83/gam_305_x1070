// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GAM305X1070_GroupCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Actor.h"
#include "TarTrap.generated.h"

UCLASS()
class GAM305X1070_GROUP_API ATarTrap : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATarTrap();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAcess = "true"))
		class UBoxComponent* TriggerBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tar Trap")
		TSubclassOf<class AGAM305X1070_GroupCharacter> player;

	//Value should be 0-1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tar Trap")
		float jumpMultiplier;

	//Value should be 0-1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tar Trap")
		float walkSpeedMultiplier;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Tar Trap")
		bool TarEffect;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float originalJumpZVelocity;
	float originalWalkSpeed;
	float reducedJumpZVelocity;
	float reducedWalkSpeed;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};
