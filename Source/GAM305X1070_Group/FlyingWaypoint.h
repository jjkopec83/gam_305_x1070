// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "FlyingWaypoint.generated.h"

/**
 * 
 */
UCLASS()
class GAM305X1070_GROUP_API AFlyingWaypoint : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	int GetFlyingWaypointOrder();

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		int FlyingWaypointOrder;
	

};
