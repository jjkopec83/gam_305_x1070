// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/Vector.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "DrawDebugHelpers.h"
#include "CollisionQueryParams.h"
#include "Components/BoxComponent.h"
#include "GAM305X1070_GroupCharacter.h"
#include "GameFramework/Actor.h"
#include "FallingSpike.generated.h"

UCLASS()
class GAM305X1070_GROUP_API AFallingSpike : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFallingSpike();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Falling Spike")
		TSubclassOf<class AGAM305X1070_GroupCharacter> Player;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Falling Spike")
		float FallSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAcess = "true"))
		class UBoxComponent* boxTrigger ;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Falling Spike")
		bool Fall;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void LineTraceForPlayer();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


};
