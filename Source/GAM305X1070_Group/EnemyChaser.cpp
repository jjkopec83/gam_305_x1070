// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyChaser.h"

// Sets default values
AEnemyChaser::AEnemyChaser()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Creates Box Collision
	boxTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Trigger"));
	boxTrigger->SetCollisionProfileName(TEXT("Trigger"));
	boxTrigger->SetupAttachment(RootComponent);
	boxTrigger->OnComponentBeginOverlap.AddDynamic(this, &AEnemyChaser::OnOverlapBegin);

	// Gets Capsule Component
	UCapsuleComponent* capsule = GetCapsuleComponent();
	capsule->OnComponentHit.AddDynamic(this, &AEnemyChaser::OnCompHit);
}

// Called when the game starts or when spawned
void AEnemyChaser::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void AEnemyChaser::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (chase) {
		FVector direction = UKismetMathLibrary::GetDirectionUnitVector(GetActorLocation(), character->GetActorLocation()) * MoveSpeed;

		AddMovementInput(direction);
		LineTraceForPlayer();
	}
}

// Called to bind functionality to input
void AEnemyChaser::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyChaser::LineTraceForPlayer()
{
	FHitResult hit;

	FVector start = GetActorLocation();
	FVector end = start + (GetActorForwardVector() * 200.f);

	//Ignore Own collision
	FCollisionQueryParams collisionParams;
	collisionParams.AddIgnoredActor(this);


	//Line Trace
	bool hitSomething = GetWorld()->LineTraceSingleByChannel(hit, start, end, ECC_WorldStatic, collisionParams);
	//DrawDebugLine(GetWorld(), start, end, FColor::Red, false, 1, 0, 1);

	if (hitSomething) {
		Jump();
	}
}

void AEnemyChaser::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->IsA(playerRef)) {
		character = OtherActor;
		UGameplayStatics::PlaySound2D(GetWorld(), Alert_Cue); // Play Alert Sound Effect
		chase = true;
		SetLifeSpan(10.f);
	}
}

void AEnemyChaser::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->IsA(playerRef)) {
		AGAM305X1070_GroupCharacter* player = Cast<AGAM305X1070_GroupCharacter>(OtherActor);
		player->UpdateHealth(-Damage);
		UGameplayStatics::PlaySound2D(GetWorld(), Explosion_Cue); // Play Explosion Sound
		Destroy();
	}
}

