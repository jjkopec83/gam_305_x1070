// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "GAM305X1070_GroupGameMode.h"
#include "GAM305X1070_GroupHUD.h"
#include "Kismet/GameplayStatics.h"
#include "GAM305X1070_GroupCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"

AGAM305X1070_GroupGameMode::AGAM305X1070_GroupGameMode()
	: Super()
{

	

	PrimaryActorTick.bCanEverTick = true;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/SideScrollerCPP/MenuCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// sets default class for blueprint health widget
	static ConstructorHelpers::FClassFinder<UUserWidget> HealthBar(TEXT("/Game/SideScrollerCPP/UI/HealthBarWidget"));
	HUDWidgetClass = HealthBar.Class;

	// use our custom HUD class
	HUDClass = AGAM305X1070_GroupHUD::StaticClass();


	// add Health Bar UI to viewport
	if (HUDWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);

		if (CurrentWidget)
		{
			CurrentWidget->AddToViewport();
		}
	}
}

void AGAM305X1070_GroupGameMode::BeginPlay()
{
	Super::BeginPlay();

	SetCurrentState(EGamePlayState::EPlaying);

	MyCharacter = Cast<AGAM305X1070_GroupCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
}

// Sets game over state if health reaches 0
void AGAM305X1070_GroupGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (MyCharacter)
	{
		if (FMath::IsNearlyZero(MyCharacter->GetHealth(), 0.001f))
		{
			SetCurrentState(EGamePlayState::EGameOver);
		}
	}
}

// Gets current gameplay state and return it
EGamePlayState AGAM305X1070_GroupGameMode::GetCurrentState() const
{
	return CurrentState;
}

void AGAM305X1070_GroupGameMode::SetGameStateToGameOver()
{
	HandleNewState(EGamePlayState::EGameOver);
}

// Sets new gameplay state upon new state realized
void AGAM305X1070_GroupGameMode::SetCurrentState(EGamePlayState NewState)
{
	CurrentState = NewState;
	HandleNewState(CurrentState);
}

float AGAM305X1070_GroupGameMode::GetScore()
{
	return Score;
}

void AGAM305X1070_GroupGameMode::AddToScore(float amountToAdd)
{
	Score += amountToAdd;
}

void AGAM305X1070_GroupGameMode::RemoveFromScore(float amountToRemove)
{
	Score -= amountToRemove;
}

// handles logic for each gameplay state using switch statement
void AGAM305X1070_GroupGameMode::HandleNewState(EGamePlayState NewState)
{
	switch (NewState)
	{
		// If gameplay state is EPlaying, do nothing
	case EGamePlayState::EPlaying:
	{
		// do nothing
	}
	break;

	// If gameplay state changes to game over, reopen level from beginning
	case EGamePlayState::EGameOver:
	{
		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	}
	break;

	// Unknown/default state
	default: // Unknown state to avoid crashes if state is unknown
	case EGamePlayState::EUnknown:
	{
		// do nothing
	}
	break;
	}
}

