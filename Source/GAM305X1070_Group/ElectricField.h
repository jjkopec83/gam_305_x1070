// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "TimerManager.h"
#include "Engine/EngineTypes.h"
#include "Components/SphereComponent.h"
#include "GAM305X1070_GroupCharacter.h"
#include "GameFramework/Actor.h"
#include "ElectricField.generated.h"

UCLASS()
class GAM305X1070_GROUP_API AElectricField : public AActor
{
	GENERATED_BODY()



public:	
	// Sets default values for this actor's properties
	AElectricField();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAcess = "true"))
		class USphereComponent* TriggerSphere;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ElectricField")
		TSubclassOf<class AGAM305X1070_GroupCharacter> player;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ElectricField")
		float tickDamage;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	AGAM305X1070_GroupCharacter* character;
	FTimerHandle TimerHandle;

	UFUNCTION()
		void DamagePlayer();

};
