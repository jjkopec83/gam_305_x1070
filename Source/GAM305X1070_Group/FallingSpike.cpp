// Fill out your copyright notice in the Description page of Project Settings.


#include "FallingSpike.h"

// Sets default values
AFallingSpike::AFallingSpike()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create Box Trigger
	boxTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Trigger"));
	boxTrigger->SetCollisionProfileName(TEXT("Trigger"));
	boxTrigger->SetupAttachment(RootComponent);
	boxTrigger->OnComponentBeginOverlap.AddDynamic(this, &AFallingSpike::OnOverlapBegin);

}

// Called when the game starts or when spawned
void AFallingSpike::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void AFallingSpike::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Fall) {
		FVector location = GetActorLocation();
		location += FVector().DownVector * FallSpeed;
		SetActorLocation(location);
	}
	else
	{
		LineTraceForPlayer();
	}

}

void AFallingSpike::LineTraceForPlayer()
{
	FHitResult hit;

	FVector start = GetActorLocation();
	FVector direction = FVector().DownVector;
	FVector end = start + (direction * 1000.f);

	//Ignore Own collision
	FCollisionQueryParams collisionParams;
	collisionParams.AddIgnoredActor(this);


	//Line Trace
	bool hitSomething = GetWorld()->LineTraceSingleByChannel(hit, start, end, ECC_Pawn, collisionParams);
	//DrawDebugLine(GetWorld(), start, hit.Location, FColor::Green, false, 1, 0, 1);

	if (hitSomething && hit.GetActor()->IsA(Player)) {
		//Debug Line
		//DrawDebugLine(GetWorld(), start, hit.Location, FColor::Red, false, 1, 0, 1);
		Fall = true;
		SetLifeSpan(5.f);
	}
}

void AFallingSpike::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//Make Sure Object that is being hit
	//Has "Generate Overlap Events" on
	//In Collsion Section
	if (OtherActor && (OtherActor != this) && OtherComp) {
		AGAM305X1070_GroupCharacter* character = Cast<AGAM305X1070_GroupCharacter>(OtherActor);
		if (character) {
			character->UpdateHealth(-character->MaxHealth);
		}

		//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *OtherActor->GetName()));
		Destroy();
	}
}
