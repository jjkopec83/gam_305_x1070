// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoKit.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "GAM305X1070_GroupCharacter.h"

// Sets default values
AAmmoKit::AAmmoKit()
{
	OnActorBeginOverlap.AddDynamic(this, &AAmmoKit::OnOverlap); // Sets overlap event for picking up ammokit objects in level environment
}

void AAmmoKit::OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor) // Define OnOverlap function that allows player to pick up ammokit 
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		class AGAM305X1070_GroupCharacter* MyCharacter = Cast<AGAM305X1070_GroupCharacter>(OtherActor);

		// If Players ammo is less than 100% then update their ammo amount by full upon picking up medkit
		if (MyCharacter && MyCharacter->GetAmmo() < 1.0f)
		{
			MyCharacter->UpdateAmmo(5.0f); // Fills Ammo back to full

			UGameplayStatics::PlaySound2D(GetWorld(), Ammo_Refill_Cue, 10.0f); // Play Ammo Refill sound effect

			//Adds score
			AGAM305X1070_GroupGameMode* gameMode = Cast<AGAM305X1070_GroupGameMode>(GetWorld()->GetAuthGameMode());
			gameMode->RemoveFromScore(FMath::FRandRange(1000, 3000));

			Destroy(); // Despawns ammokit on pickup
		}
	}
}
