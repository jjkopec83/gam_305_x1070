// Fill out your copyright notice in the Description page of Project Settings.


#include "FlyingEnemyAI.h"

// Sets default values
AFlyingEnemyAI::AFlyingEnemyAI()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFlyingEnemyAI::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFlyingEnemyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

