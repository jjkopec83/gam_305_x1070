// Fill out your copyright notice in the Description page of Project Settings.


#include "TileGenerator.h"

// Sets default values
ATileGenerator::ATileGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATileGenerator::BeginPlay()
{
	Super::BeginPlay();
	spawnLocation = firstTIle->GetActorLocation();
	StartPosition = spawnLocation.Y;
	spawnLocation.Y -= tileSize;
	PlacedTile = firstTIle;
	EndPosition =  -(tileSize * (genAmount + 1));
	levelProgressPercent = 0.f;

}

// Called every frame
void ATileGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector actorLocation = followActor->GetActorLocation();

	if (spawnLocation.Y + (tileSize * 1.5) > actorLocation.Y) {
		int randIndex = FMath::RandRange(0, levelTiles.Num()-1);

		if (branchType != 0 && levelTiles[randIndex].GetDefaultObject()->GetBranchType() != 0)
			return;
		if (genAmount > 0) {
			PlacedTile = Cast<ALevelTile>(GetWorld()->SpawnActor(levelTiles[randIndex], &spawnLocation));	//PlacedTile formerly entered as 'tile' -JK
#if WITH_EDITOR
			PlacedTile->SetFolderPath(GetFolderPath());  //Spawns actor in this Actor's folder
#endif
		}
		else 
			GetWorld()->SpawnActor(lastTile, &spawnLocation);

		genAmount--;
		spawnLocation.Y -= tileSize * levelTiles[randIndex].GetDefaultObject()->GetTileWidth();
		branchType = PlacedTile->GetBranchType();	//PlacedTile formerly entered as 'tile' -JK

		//Adds score
		AGAM305X1070_GroupGameMode* gameMode = Cast<AGAM305X1070_GroupGameMode>(GetWorld()->GetAuthGameMode());
		gameMode->AddToScore(FMath::FRandRange(1000, 3500));
	}
	else if (fabsf(actorLocation.Z - spawnLocation.Z) > tileSize / 2) {
		int randIndex = FMath::RandRange(0, levelTiles.Num()-1);
		spawnLocation.Z += tileSize * UKismetMathLibrary::SignOfFloat(actorLocation.Z - spawnLocation.Z);
		spawnLocation.Y += tileSize;
		if (genAmount > 0) {
			if (levelTiles[randIndex].GetDefaultObject()->GetBranchType() == 0) {
				PlacedTile = Cast<ALevelTile>(GetWorld()->SpawnActor(levelTiles[randIndex], &spawnLocation));	//PlacedTile formerly entered as 'tile' -JK
#if WITH_EDITOR
				PlacedTile->SetFolderPath(GetFolderPath()); //Spawns actor in this Actor's folder
#endif
			}
		}
		else 
			GetWorld()->SpawnActor(lastTile, &spawnLocation);
		spawnLocation.Y -= tileSize * levelTiles[randIndex].GetDefaultObject()->GetTileWidth();

		//Adds score
		AGAM305X1070_GroupGameMode* gameMode = Cast<AGAM305X1070_GroupGameMode>(GetWorld()->GetAuthGameMode());
		gameMode->AddToScore(FMath::FRandRange(1000, 3500));
	}

	//Calculate Level Percent
	levelProgressPercent = (actorLocation.Y) / (EndPosition);

}


float ATileGenerator::GetLevelPercent()
{
	return levelProgressPercent;
}