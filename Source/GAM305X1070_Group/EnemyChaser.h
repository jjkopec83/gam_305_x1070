// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Sound/SoundBase.h"
#include "DrawDebugHelpers.h"
#include "CollisionQueryParams.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "GAM305X1070_GroupCharacter.h"
#include "Components/CapsuleComponent.h"
#include "EnemyChaser.generated.h"

UCLASS()
class GAM305X1070_GROUP_API AEnemyChaser : public ACharacter
{
	GENERATED_BODY()


public:
	// Sets default values for this character's properties
	AEnemyChaser();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAcess = "true"))
		class UBoxComponent* boxTrigger;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chaser Enemy")
		TSubclassOf<class AGAM305X1070_GroupCharacter> playerRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chaser Enemy")
		float MoveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chaser Enemy")
		float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chaser Enemy") //Explosion Sound
		USoundBase* Explosion_Cue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Chaser Enemy") //Explosion Sound
		USoundBase* Alert_Cue;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Chaser Enemy")
		bool chase;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Chaser Enemy")
		AActor* character;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void LineTraceForPlayer();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

};
