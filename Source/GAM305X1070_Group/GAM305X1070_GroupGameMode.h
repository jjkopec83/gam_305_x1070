// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAM305X1070_GroupCharacter.h"
#include "GAM305X1070_GroupGameMode.generated.h"

//enum to store the current state of gameplay
UENUM()
enum class EGamePlayState
{
	EPlaying,
	EGameOver,
	EUnknown
};

UCLASS(minimalapi)
class AGAM305X1070_GroupGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAM305X1070_GroupGameMode();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override; // checks if player is dead or not

	AGAM305X1070_GroupCharacter* MyCharacter; // Create character object based on player character

	/** Returns the current playing state */
	UFUNCTION(BlueprintPure, Category = "Health")
		EGamePlayState GetCurrentState() const;

	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetGameStateToGameOver();

	/** Sets a new playing state */
	void SetCurrentState(EGamePlayState NewState);

	/** Create Health bar class */
	UPROPERTY(EditAnywhere, Category = "Health")
		TSubclassOf<class UUserWidget> HUDWidgetClass;

	/** Creates health bar widget */
	UPROPERTY(EditAnywhere, Category = "Health")
		class UUserWidget* CurrentWidget;

	UFUNCTION(BlueprintPure, Category = "Score")
		float GetScore();

	UFUNCTION(BlueprintCallable, Category = "Score")
		void AddToScore(float amountToAdd);

	UFUNCTION(BlueprintCallable, Category = "Score")
		void RemoveFromScore(float amountToRemove);

private:

	/**Keeps track of the current playing state */
	EGamePlayState CurrentState;

	float Score;

	/** Handle any function calls that rely upon changing the playing state of our game */
	void HandleNewState(EGamePlayState NewState);

};


