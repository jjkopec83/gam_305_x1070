// Fill out your copyright notice in the Description page of Project Settings.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GAM305X1070_GroupHUD.generated.h"

UCLASS()
class AGAM305X1070_GroupHUD : public AHUD
{
	GENERATED_BODY()

public:
	AGAM305X1070_GroupHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;


};
