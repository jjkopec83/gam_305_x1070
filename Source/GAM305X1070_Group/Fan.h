// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/Actor.h"
#include "CollisionQueryParams.h"
#include "Kismet/KismetMathLibrary.h"
#include "GAM305X1070_GroupCharacter.h"
#include "Fan.generated.h"

UCLASS()
class GAM305X1070_GROUP_API AFan : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFan();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fan")
		TSubclassOf<class AGAM305X1070_GroupCharacter> PlayerRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fan")
		float DetectDistance = 200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fan")
		float PushForce = 8000.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void BoxTraceForPlayer();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
